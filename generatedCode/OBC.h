#ifndef _OBC_H
#define _OBC_H


#include <string>
using namespace std;

class ADCS;
class EPS;
class EDT;
class TCS;

class OBC {
  private:
    string debug;

    string earth_command;

    bool profileTCS_command;

    int shutdown_module;

    int activate_module;


  public:
    void manageEnergy();


  private:
    ADCS * ;

    EPS * ;

    TCS * ;

};
#endif
